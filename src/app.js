import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-rtl/dist/css/bootstrap-rtl.min.css';
import 'sweetalert2/dist/sweetalert2.min.css';
import 'src/css/style.css';
import router from "./router";
import {sync} from "vuex-router-sync";
import Vue from "vue";
import VueRouter from "vue-router";
import store from "./store";
import {setRouter} from "./services/MyAxios";
import AuthModule from "./store/modules/Auth.module";


Vue.use(VueRouter);
sync(store, router);
setRouter(router);


const app = new Vue({
	store,
	router,
	render: function (ce) {
		return ce(require("./App.vue"));
	},
	components: {},
}).$mount('#app');

document.addEventListener('deviceready', function () {
	initializeOneSignal();
}, true);

function initializeOneSignal() {
	window.plugins.OneSignal
		.startInit("7ab4231e-4748-4cae-acde-afe5f0cb2bd8") // app Id in OneSignal system
		.handleNotificationOpened(notificationOpenedCallback)
		.inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.Notification)
		.endInit();

	// store register ID
	window.plugins.OneSignal.getIds(function (ids) {
		localStorage.OS_id = ids.userId;
		store.dispatch(AuthModule.types.LOGIN, {id: ids.userId, gender: localStorage.gender});
	});
}

function notificationOpenedCallback(object) {
	console.log(object.notification);
	if (object.notification.payload.launchURL || object.notification.payload.body == "תזכורת מותאמת אישית על הלימוד היומי") {
		router.push("/");
	}
	else {
		localStorage.alertBody = object.notification.payload.body;
		router.push("/alert");
	}


}