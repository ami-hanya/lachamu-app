import BaseService from "./BaseService";
import axios from "./MyAxios";

export default class QuestionService extends BaseService {
    get className() {
        return "users";
    }

    login(params) {
        return axios.post("login", params).then(
            (response) => {
                return response.data;
            }
        );
    }

    notification(params) {
        return axios.post("notification", params).then(
            (response) => {
                return response.data;
            }
        );
    }
};