/**
 * Created by Matanya on 26-Nov-17.
 */
import BaseService from "./BaseService";
import axios from "./MyAxios";

export default class ContactService extends BaseService {
    get className() {
        return "contact";
    }
}