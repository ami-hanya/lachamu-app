class CacheStorage {


    constructor(uniqueId = null) {
        this._uniqueId = uniqueId;
        this._storage = new Map(this._getFromLocalStorage());
    }

    get storage() {
        return this._storage;
    }

    store(key, value) {
        this.storage.set(CacheStorage._createKeyString(key), value);
        this._saveToLocalStorage();

    }

    getFromStore(key, promise = null) {
        let storage = null;
        if (this.storage.has(CacheStorage._createKeyString(key))) {
            storage = this.storage.get(CacheStorage._createKeyString(key));
        }
        if (storage) {
            return storage;
        }
        if (promise) {
            return promise;
        }
        return null;
    }

    static _createKeyString(key) {
        if (typeof key === "object") {
            return JSON.stringify(key);
        }
        return key;
    }

    toString() {
        return JSON.stringify([...this.storage]);
    }

    _saveToLocalStorage() {
        if (this._uniqueId) {
            localStorage.setItem(this._uniqueId, this.toString());
        }
    }

    _getFromLocalStorage() {
        if (this._uniqueId) {
            return JSON.parse(localStorage.getItem(this._uniqueId));
        }
    }


}

export default CacheStorage; 