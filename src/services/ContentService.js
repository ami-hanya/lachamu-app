/**
 * Created by Matanya on 26-Nov-17.
 */
import BaseService from "./BaseService";
import axios from "./MyAxios";

export default class ContactService extends BaseService {
    get className() {
        return "times";
    }
    get(params = {}) {
        return axios.get(this.className, {params}).then(
            response => {
                if (response.data) {
                    return response.data;
                } else {
                    throw new Error(response.data);
                }

            }
        );
    }
}