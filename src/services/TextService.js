/**
 * Created by Lior on 04/02/2018.
 */
import BaseService from "./BaseService";
import axios from "./MyAxios";

export default class TextService extends BaseService {
    get className() {
        return "article";
    }

    get(params = {}) {
        return axios.get(this.className, {params}).then(
            response => {
                if (response.data) {
                    return response.data;
                } else {
                    throw new Error(response.data);
                }

            }
        );
    }

    fetch(params = {}) {
        return axios.get(this.className, {params}).then(
            response => {
                if (response.data) {
                    return response.data;
                } else {
                    throw new Error(response.data);
                }

            }
        );
    }

}


