/**
 * Created by elad on 20 נובמבר 2016.
 */
import axios from "./MyAxios";

export default class BaseService {

	create(data) {
		return axios.post(this.className, data).then(
			(response) => {
				return response.data;
			}
		);
	}

	update(id, data) {
		if (!data._method) {
			data._method = "put";
		}
		return axios.post(this.className + "/" + id, data).then(
			(response) => {
				return response.data;
			}
		);
	}

	fetch(params = {}) {
		return axios.get(this.className, {params}).then(
			(response) => {
				return response.data;
			}
		);
	}

	get(id, params = {}) {
		return axios.get(this.className + "/" + id, {params}).then(
			response => {
				if (response.data) {
					return response.data;
				} else {
					throw new Error(response.data);
				}

			}
		);
	}

	remove(id, dataUrl = {}) {
		if (!dataUrl._method) {
			dataUrl._method = "delete";
		}
		return axios.post(this.className + "/" + id, dataUrl).then(
			response => {
				if (response.data) {
					return response.data;
				} else {
					throw new Error(response.data);
				}

			}
		);
	}

	save(data) {
		if (data.id) {
			return this.update(data.id, data);
		} else {
			return this.create(data);
		}
	}


	_errorFunction() {

	}

	get className() {
		return null;
	}


	static removeFromArray(arr, obj) {
		let clone = arr.slice(0);
		let index = clone.indexOf(obj);
		if (index > -1) {
			clone.splice(index, 1);
		}
		return clone;
	}


	static removeFromArryById(arr, id) {
		let founded = false
		arr.forEach((o, index, object) => {
			if (o.id == id) {
				object.splice(index, 1);
				founded = true
				return founded
			}
		})
		return founded
	}

	static UpdateArray(arr, obj) {
		let index = arr.indexOf(obj);
		if (index > -1) {
			arr[index] = obj;
			return true;
		}
		return false;
	}


	static UpdateArrayById(models, obj) {
		let clone = models.slice(0);
		let model = clone.find((model) => {
			return model.id === obj.id;
		});
		if (model) {
			Object.assign(model, obj);
		}
		return clone;

	}


};