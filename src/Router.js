import VueRouter from "vue-router";
import DashboardView from "./view/pages/DashboardView.vue";
import AboutView from "./view/pages/AboutView.vue";
import TimesView from "./view/pages/TimesView.vue";
import SettingsView from "./view/pages/SettingsView.vue";
import ContactView from "./view/pages/ContactView.vue";
import LearnView from "./view/pages/LearnView.vue";
import AskTheRabbi from "./view/pages/AskTheRabbi.vue";
import AlertView from "./view/pages/AlertView";


const routes = [
    {path: '/dash', component: DashboardView, name: "dashBoard"},
    {path: '/about', component: AboutView, name: "about"},
    {path: '/times', component: TimesView, name: "times"},
    {path: '/settings', component: SettingsView, name: "settings"},
    {path: '/contact', component: ContactView, name: "contact"},
    {path: '/AskTheRabbi', component: AskTheRabbi, name: "askRabbi"},
    {path: '/learn', component: LearnView, name: "learn"},
    {path: '/alert', component: AlertView, name: "alert"},
    {path: '/', redirect: '/learn', name: "home"},
];

const router = new VueRouter({
    history: false,
    routes
});

export default router;
