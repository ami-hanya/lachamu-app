import BaseService from "./BaseService";
import axios from "./MyAxios";

export default class QuestionService extends BaseService {
	get className() {
		return "users";
	}


	login(params) {
		return axios.post("login", params).then(
			response => {
				return response.data;
			}
		);
	}

	getAuth() {
		return axios.get("auth").then(
			response => {
				return response.data;
			}
		);
	}

	logout() {
		return axios.get("logout").then(
			response => {
				return response.data;
			}
		);
	}


	checkMail(email) {
		return axios.get("users/exist", {params: {email}}).then(
			response => {
				return response.data;
			}
		);
	}
};