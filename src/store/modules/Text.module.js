/**
 * Created by Lior on 04/02/2018.
 */
import TextService from "../../services/TextService";

export const types = {
    ARTICLE: "text/article",
};

const service = new TextService();

export default {
    state: {
        texts: [],
        text: {},
    },

    mutations: {

        [types.ARTICLE](state, texts) {
            state.texts = texts;
        },
    },

    actions: {
        [types.ARTICLE](state, form){
            return service.fetch(form).then((response) => {
                state.commit(types.ARTICLE, response);

                return response;
            });
        },
    },
    types: types
}