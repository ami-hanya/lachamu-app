/**
 * Created by Matanya on 27-Nov-17.
 */
import ContentService from "../../services/ContentService";

export const types = {
    TIMES: "content/date",
};

const service = new ContentService();

export default {
    actions: {
        [types.TIMES](state, form){
            return service.get(form).then((response) => {
                return response;
            });
        },
    },
    types: types
}