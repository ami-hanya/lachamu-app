/**
 * Created by Matanya on 26-Nov-17.
 */
import ContactService from "../../services/ContactService";

export const types = {
    SEND: "contact/send",
};

const service = new ContactService();

export default {
    actions: {
        [types.SEND](state, form){
            service.create(form).then((response) => {
                return response;
            });
        }
    },
    types: types
}