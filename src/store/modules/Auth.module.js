import UserService from "../../services/AuthService";

export const types = {
	LOGIN: "auth/login",
	GENDER: "auth/gender",
	NOTIFICATION: "auth/notification",
};
const service = new UserService;
export default {

	state: {
		user: {
			gender: localStorage.gender,
			OS_id: localStorage.OS_id,
			hour_reminder: '09:00',
			notification: 2
		}
	},

	mutations: {
		[types.LOGIN](state, userLogin) {
			state.user = Object.assign({}, userLogin);
		},
		[types.GENDER](state, gender) {
			state.user.gender = gender;
		},
		[types.NOTIFICATION](state, data) {
			state.user.hour_reminder = data.hour_reminder;
			state.user.notification = data.notification;
		},
	},

	actions: {
		[types.LOGIN](state, data) {
			localStorage.gender = data.gender;
			state.commit(types.GENDER, data.gender);
			return service.login(data).then((response) => {
				if (response) {
					state.commit(types.LOGIN, response);
				}
				return response;
			});
		},
		[types.NOTIFICATION](state, data) {
			return service.notification(data).then((response) => {
				state.commit(types.NOTIFICATION, data);
				return response;
			});
		},
	},

	types: types
}