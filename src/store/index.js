import Vue from "vue";
import Vuex from "vuex";
import AuthModule from "./modules/Auth.module";
import ContactModule from "./modules/Contact.module";
import ContentModule from "./modules/Content.module";
import TextModule from "./modules/Text.module";


Vue.use(Vuex);
const store = new Vuex.Store({
    state: {},
    modules: {
        AuthModule,
        ContactModule,
        ContentModule,
        TextModule,
    }
});
window.ss = store;
export default store;