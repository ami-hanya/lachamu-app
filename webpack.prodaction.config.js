var webpack = require("webpack");
var path = require("path");

function resolve(dir) {
	return path.join(__dirname, '..', dir)
}

module.exports = {
	entry: ["babel-polyfill", "./dev/app.js"],
	output: {
		path: path.resolve(__dirname, "public/js/"),
		filename: "prod.js",
		sourceMapFilename: "prod.js.map"
	},
	resolve: {
		extensions: ['.js', '.vue', '.json'],
		alias: {
			'vue$': 'vue/dist/vue.esm.js',
			'@': resolve('src')
		}
	},
	module: {
		rules: [
			{test: /\.js$/, exclude: /node_modules/, loader: "babel-loader"},
			{test: /\.vue/, loader: "vue-loader"},
			{test: /\.css$/, use: [{loader: "style-loader"}, {loader: "css-loader"}]},
			{test: /\.html$/, loader: "html-loader"},
			{test: /\.jpg$/, loader: "file-loader"},
			{test: /\.png$/, loader: "url-loader?mimetype=image/png"},
			{test: /\.(woff|woff2)$/, loader: "url-loader?limit=10000&mimetype=application/font-woff"},
			{test: /\.ttf$/, loader: "file-loader"},
			{test: /\.eot$/, loader: "file-loader"},
			{test: /\.svg$/, loader: "file-loader"}
		]
	},
	plugins: [
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery",
			moment: "moment",
			swal: 'sweetalert2'
		}),
		new webpack.DefinePlugin({
			'process.env': {
				'DEBUG': false,
				NODE_ENV: '"production"'
			}
		}),
		new webpack.optimize.CommonsChunkPlugin({
			name: 'vendor',
			filename: 'vendor.js',
			minChunks(module, count) {
				var context = module.context;
				return context && context.indexOf('node_modules') >= 0;
			},
		}),


	]
};